﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OTEL.Models;

namespace OTEL.Models
{
    public class rezekle : Controller
    {
        public int R_NO { get; set; }
        public string AD { get; set; }
        public string SOYAD { get; set; }
        public string TELEFON { get; set; }
        public string EMAIL { get; set; }
        public int KISISAYISI { get; set; }
        


        [AllowHtml]
        public string ODA { get; set; }
        public static bool kayitEkle(int R_NO, string AD, string SOYAD, string TELEFON,string EMAIL, int KISISAYISI, string ODA)
        {

            using (otelrezEntities db = new otelrezEntities()) //Entity sayesinde veritabanına ulaşabiliyoruz
            {
                REZTABLO kayit = new REZTABLO();  // (code_table) tablo ismimiz
                kayit.R_NO = R_NO;
                kayit.AD = AD;
                kayit.SOYAD = SOYAD;
                kayit.TELEFON = TELEFON;
                kayit.EMAIL = EMAIL;
                kayit.KISISAYISI = KISISAYISI;
                kayit.ODA = ODA;
                db.REZTABLOes.Add(kayit); // tabloya <strong>kodEkleController </strong>dan gelen değerleri ekliyoruz
                db.SaveChanges(); // ve kaydediyoruz
                return true;

            }
        }

    }
}