﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OTEL.Models;

namespace OTEL.Controllers
{
    public class rezekleController : Controller
    {
        // GET: rezekle
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult kaydet(rezekle model)
        {
            rezekle.kayitEkle(model.R_NO, model.AD, model.SOYAD, model.TELEFON, model.EMAIL, model.KISISAYISI, model.ODA); // kullanıcıdan aldığımız değerleri veritabanına ekliyoruz
            return View();
        }
    }
}