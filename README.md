Hocam PDF dökümanında belirttiğim sitenin REZERVASYON sayfasında veri tabanını textboxlara bağlamaya çalıştığım ama başaramadığım kodlar da burada belki incelemek istersiniz.



    @model OTEL.Models.rezekle
    @{
        ViewBag.Title = "blog";
        Layout = "~/Views/Shared/_Layout.cshtml";
        <link href="~/style.css" rel="stylesheet" />
    }

    @using (Html.BeginForm("kaydet", "rezekle", FormMethod.Post))
    {
        <form method="post" autocomplete="on">
            <table>
                <tr>
                
                    <td>
					 <td><p><label>Ad: </label></p></td>
                        <p>
                            <label>
                                @Html.TextBoxFor(x => x.AD) (Adınız) <br />
                            </label>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td><p><label>Soyad: </label></p></td>
                    <td>
                        <p>
                            <label>
                                @Html.TextBoxFor(x => x.SOYAD) (Soyadınız) <br />
                            </label>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td><p><label>Telefon: </label></p></td>
                    <td>
                        <p>
                            <label>
                                @Html.TextAreaFor(x => x.TELEFON  (Telefon))<br />
                            </label>
                        </p>
                    </td>
                </tr>
				  <tr>
                    <td><p><label>Email: </label></p></td>
                    <td>
                        <p>
                            <label>
                                @Html.TextAreaFor(x => x.EMAIL  (email))<br />
                            </label>
                        </p>
                    </td>
                </tr>
				  <tr>
                    <td><p><label>Kisisayisi: </label></p></td>
                    <td>
                        <p>
                            <label>
                                @Html.TextAreaFor(x => x.KISISAYISI  (Odada kalacak kişi sayısı))<br />
                            </label>
                        </p>
                    </td>
                </tr>
				  <tr>
                    <td><p><label>Oda: </label></p></td>
                    <td>
                        <p>
                            <label>
                                @Html.TextAreaFor(x => x.ODA  (ODA TÜRÜ: JS-D-E))<br />
                            </label>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="Text-align:right;">
                        <p>
                            <input type="submit" value="kaydet" />
                            <input type="reset" value="Temizle" />
                        </p>
                    </td>
                </tr>
            </table>
        </form>

    }